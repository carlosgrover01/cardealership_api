﻿using CarDealership.DTO;
using CarDealership.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace CarDealership.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CarsController : Controller
    {
        [HttpGet]
        public IEnumerable<CarDto> Get()
        {
            using var context = new SQL2017_721246_juliomrContext();
            return context.Cars
                           .Include(b => b.CarBrand)
                           .Include(b => b.CarModel)
                           .Include(b => b.CarType)
                           .Select(x => new CarDto
                           {
                               Id = x.Id,
                               Brand = x.CarBrand.Name,
                               Model = x.CarModel.Name,
                               Type = x.CarType.Name
                           }).ToList();
        }
    }
}
