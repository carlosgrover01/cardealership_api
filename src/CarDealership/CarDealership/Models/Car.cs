﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CarDealership.Models
{
    public partial class Car
    {
        public Car()
        {
            RentalTransactions = new HashSet<RentalTransaction>();
        }

        public int Id { get; set; }
        public int CarTypeId { get; set; }
        public int CarBrandId { get; set; }
        public int CarModelId { get; set; }
        public bool IsAvailable { get; set; }
        public int? CurrentRentalTransactionId { get; set; }

        public virtual CarBrand CarBrand { get; set; }
        public virtual CarModel CarModel { get; set; }
        public virtual CarType CarType { get; set; }
        public virtual RentalTransaction CurrentRentalTransaction { get; set; }
        public virtual ICollection<RentalTransaction> RentalTransactions { get; set; }
    }
}
