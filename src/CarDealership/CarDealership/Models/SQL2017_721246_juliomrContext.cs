﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace CarDealership.Models
{
    public partial class SQL2017_721246_juliomrContext : DbContext
    {
        public SQL2017_721246_juliomrContext()
        {
        }

        public SQL2017_721246_juliomrContext(DbContextOptions<SQL2017_721246_juliomrContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Car> Cars { get; set; }
        public virtual DbSet<CarBrand> CarBrands { get; set; }
        public virtual DbSet<CarModel> CarModels { get; set; }
        public virtual DbSet<CarType> CarTypes { get; set; }
        public virtual DbSet<RentalTransaction> RentalTransactions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=sql2k1701.discountasp.net; Database=SQL2017_721246_juliomr; User=juliomr1; Password=no1Knows$;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("cr")
                .HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Car>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CarBrandId).HasColumnName("carBrandId");

                entity.Property(e => e.CarModelId).HasColumnName("carModelId");

                entity.Property(e => e.CarTypeId).HasColumnName("carTypeId");

                entity.Property(e => e.CurrentRentalTransactionId).HasColumnName("currentRentalTransactionId");

                entity.Property(e => e.IsAvailable).HasColumnName("isAvailable");

                entity.HasOne(d => d.CarBrand)
                    .WithMany(p => p.Cars)
                    .HasForeignKey(d => d.CarBrandId)
                    .HasConstraintName("FK_Cars_CarBrands");

                entity.HasOne(d => d.CarModel)
                    .WithMany(p => p.Cars)
                    .HasForeignKey(d => d.CarModelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Cars_CarModels");

                entity.HasOne(d => d.CarType)
                    .WithMany(p => p.Cars)
                    .HasForeignKey(d => d.CarTypeId)
                    .HasConstraintName("FK_Cars_CarTypes");

                entity.HasOne(d => d.CurrentRentalTransaction)
                    .WithMany(p => p.Cars)
                    .HasForeignKey(d => d.CurrentRentalTransactionId)
                    .HasConstraintName("FK_Cars_RentalTransactions");
            });

            modelBuilder.Entity<CarBrand>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<CarModel>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<CarType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<RentalTransaction>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CarId).HasColumnName("carId");

                entity.Property(e => e.DateRented)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("dateRented");

                entity.Property(e => e.DateReturned)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("dateReturned");

                entity.HasOne(d => d.Car)
                    .WithMany(p => p.RentalTransactions)
                    .HasForeignKey(d => d.CarId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RentalTransactions_Cars");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
