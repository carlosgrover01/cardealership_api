﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CarDealership.Models
{
    public partial class RentalTransaction
    {
        public RentalTransaction()
        {
            Cars = new HashSet<Car>();
        }

        public int Id { get; set; }
        public int CarId { get; set; }
        public DateTime DateRented { get; set; }
        public DateTime? DateReturned { get; set; }

        public virtual Car Car { get; set; }
        public virtual ICollection<Car> Cars { get; set; }
    }
}
